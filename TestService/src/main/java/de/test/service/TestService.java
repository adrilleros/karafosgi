package de.test.service;

import de.test.api.ITestService;

public class TestService implements ITestService{

	public void startUp(){
		System.out.println("StartUP TestService");
	}

	@Override
	public void sayHello() {
		System.out.println(" >> Hello from TestService <<");
		
	}
}
