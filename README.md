Testing some OSGi Bundles to be deployed in Apache Karaf
====================================================

- TestParent is the Parent-Project. Running a Maven-Install on it will build the Bundle jars in their corresponding target Folder
- Testing is is done in the Project PaxTest. Pax Exam will download an Apache Karaf Installation from Maven and install the defined features and plugins to it.

If you want to deploy it without testing. Simply download an Apache Karaf binary distirbution and copy the bundle jars into its deploy folder.