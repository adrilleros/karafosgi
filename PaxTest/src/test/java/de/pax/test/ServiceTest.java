package de.pax.test;

import static org.junit.Assert.assertNotNull;
import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;

import javax.inject.Inject;

import org.junit.Test;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.options.MavenUrlReference;

import de.test.api.ITestService;

public class ServiceTest extends AbstractPaxTest{
	@Inject
	ITestService testService;
	
	@Override
    protected Option[] testOptions() {
		MavenUrlReference karafCxfRepo = maven("org.apache.cxf.karaf", "apache-cxf", KARAF_CXF_VERSION)
				.classifier("features")
				.type("xml");
		
       return new Option[] {
            // KarafDistributionOption.debugConfiguration("5005", true),
			features(karafCxfRepo, "cxf-jaxrs"),
            mavenBundle()
                .groupId("TestService")
                .artifactId("TestService")
                .start(),
            mavenBundle()
                .groupId("TestApi")
                .artifactId("TestApi")
                .start()
       };
    }


	@Test
    public void serviceTest() {
		assertNotNull(testService);
		testService.sayHello();
	}
	
}
