package de.pax.test;

import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.CoreOptions.options;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.configureConsole;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.karafDistributionConfiguration;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.keepRuntimeFolder;

import java.io.File;

import org.junit.runner.RunWith;
import org.ops4j.pax.exam.Configuration;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.junit.PaxExam;
import org.ops4j.pax.exam.karaf.options.KarafDistributionConfigurationFileReplacementOption;
import org.ops4j.pax.exam.options.DefaultCompositeOption;
import org.ops4j.pax.exam.options.MavenArtifactUrlReference;

@RunWith(PaxExam.class)
public abstract class AbstractPaxTest {
	
	/*
	 * Make sure that the Version are compatible to each other
	 */
	public static final String KARAF_VERSION = "3.0.3";
	public static final String KARAF_FEATURES_VERSION = "3.0.3";
	public static final String KARAF_CXF_VERSION = "3.0.3";
	public static final String KARAF_CAMEL_VERSION = "2.14.1";

	@Configuration
    public Option[] config() {
		DefaultCompositeOption compositeTestOptions = new DefaultCompositeOption();
		compositeTestOptions.add(testOptions());
		
        return options(
            karafDistributionConfiguration()
                .frameworkUrl(getKarafUrl())
                .unpackDirectory(new File("target/exam"))
                .useDeployFolder(false)
                .karafVersion(KARAF_VERSION),
            keepRuntimeFolder(),
            configureConsole().ignoreLocalConsole(),
			new KarafDistributionConfigurationFileReplacementOption("etc/test_rest.cfg", new File("../TestParent/src/main/config/test_rest.cfg")),
            compositeTestOptions
       );
    }
	
	private MavenArtifactUrlReference getKarafUrl(){
		String fileType = null;
		String os = System.getProperty("os.name");
		if(os.startsWith("Windows")){
			fileType = "zip";
		}else if(os.startsWith("Linux") || os.startsWith("LINUX")){
			fileType = "tar.gz";
		}
		
		
		return maven("org.apache.karaf", "apache-karaf", KARAF_VERSION)
	          .type(fileType);
		
	}
	
	/**
	 * Add here the needed {@code KarafFeaturesOption} 
	 * and the needed {@code MavenArtifactProvisionOption} bundles 
	 * for this test
	 * @return an array of Options (e. g. karaf features and bundles)
	 */
	protected abstract Option[] testOptions();

}
