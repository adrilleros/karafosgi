package de.pax.test;

import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;

import java.util.ArrayList;
import java.util.List;

import org.apache.cxf.jaxrs.client.WebClient;
import org.codehaus.jackson.jaxrs.JacksonJsonProvider;
import org.junit.Test;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.options.MavenUrlReference;

import de.test.api.model.TestModel;

public class CXFTest  extends AbstractPaxTest{
	
	@Override
	protected Option[] testOptions() {
		MavenUrlReference karafCxfRepo = maven("org.apache.cxf.karaf", "apache-cxf", KARAF_CXF_VERSION)
	            						.classifier("features")
	            						.type("xml");
	        

	        
	    MavenUrlReference karafStandardRepo = maven("org.apache.karaf.features", "standard", KARAF_FEATURES_VERSION)
	                						 .classifier("features")
	                						 .type("xml");
	    
	    
		return new Option[] {
	            // KarafDistributionOption.debugConfiguration("5005", true),
				features(karafStandardRepo, "war"),
				features(karafCxfRepo, "cxf-jaxrs"),
				mavenBundle("org.codehaus.jackson", "jackson-core-asl"),
				mavenBundle("org.codehaus.jackson", "jackson-mapper-asl"),
				mavenBundle("org.codehaus.jackson", "jackson-jaxrs"),
				mavenBundle("TestApi", "TestApi").start(),
				mavenBundle("TestCXF", "TestCXF").start()};
	}
	
	@Test
	public void tetsWithClient() {
		List<Object> providers = new ArrayList<Object>();
	    providers.add( new JacksonJsonProvider() );
		
	    WebClient client = WebClient.create("http://localhost:8181/cxf", providers);
		client.accept("application/json");
		client.path("pojo");
		TestModel getResult = client.get(TestModel.class);
		System.err.println("Get Result: " + getResult);
	}

}
