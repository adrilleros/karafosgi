package de.pax.test;

import static org.ops4j.pax.exam.CoreOptions.maven;
import static org.ops4j.pax.exam.CoreOptions.mavenBundle;
import static org.ops4j.pax.exam.karaf.options.KarafDistributionOption.features;

import org.apache.cxf.jaxrs.client.WebClient;
import org.junit.Test;
import org.ops4j.pax.exam.Option;
import org.ops4j.pax.exam.options.MavenUrlReference;

public class CamelRestTest extends AbstractPaxTest{
	
	@Override
	protected Option[] testOptions() {
		MavenUrlReference karafCxfRepo = maven("org.apache.cxf.karaf", "apache-cxf", KARAF_CXF_VERSION)
	            						.classifier("features")
	            						.type("xml");
	        
	    MavenUrlReference karafCamelRepo = maven("org.apache.camel.karaf", "apache-camel", KARAF_CAMEL_VERSION)
	                					  .classifier("features")
	                					  .type("xml");
	        
	    MavenUrlReference karafStandardRepo = maven("org.apache.karaf.features", "standard", KARAF_FEATURES_VERSION)
	                						 .classifier("features")
	                						 .type("xml");
	    
	    
		return new Option[] {
	            // KarafDistributionOption.debugConfiguration("5005", true),
				features(karafStandardRepo, "war"),
				features(karafCxfRepo, "cxf"),
				features(karafCamelRepo, "camel-blueprint", "camel-cxf", "camel-jackson"),
				mavenBundle("TestApi", "TestApi").start(),
				mavenBundle("TestCamelRest", "TestCamelRest").start() };
	}
	
	@Test
	public void tetsWithClient() {
		WebClient client = WebClient.create("http://localhost:8080");
		client.accept("application/json");
		client.path("rest");
		String getResult = client.get(String.class);
		System.err.println("Get Result: " + getResult);
	}


	
	
}
