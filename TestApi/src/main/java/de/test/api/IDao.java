package de.test.api;

public interface IDao<T> {

	/**
	 * Save new and update
	 * 
	 * @param daoObject
	 * @return
	 */
	public boolean merge(T daoObject);
	
	public boolean delete(T daoObject);
	
	/**
	 * 
	 * @param id
	 * @return the daoObject with the given id
	 */
	public T read(Long id);
}
