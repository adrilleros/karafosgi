package de.test.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("rest")
@Consumes("application/json")
@Produces("application/json")
public interface IRest {
	
	@GET
	public String testRest();

}
