package de.test.api.rest;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import de.test.api.model.TestModel;

@Path("pojo")
@Consumes("application/json")
@Produces("application/json")
public interface IRestPojo {

	@GET
	public TestModel getTestModel();
	
}
