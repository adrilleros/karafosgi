package de.test.cxf;

import de.test.api.model.TestModel;
import de.test.api.rest.IRestPojo;

public class CXFRestService implements IRestPojo{

	@Override
	public TestModel getTestModel() {
		TestModel testModel = new TestModel();
		testModel.setId(3442L);
		testModel.setName("TestModelName");
		return testModel;
	}

}
