package de.test.camel.rest;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.component.cxf.common.message.CxfConstants;

public class RestProcessor implements Processor{

	@Override
	public void process(Exchange exchange) throws Exception {
		
		// TODO Auto-generated method stub
		Message inMessage = exchange.getIn(); 
		String operationName = inMessage.getHeader(CxfConstants.OPERATION_NAME, String.class);
		if("testRest".equals(operationName)){
			Customer customer = new Customer();
			customer.setName("Harold Cooper");
			customer.setCustomerId("3253456ZZZ");
			exchange.getOut().setBody(customer);
		}
		
		// The parameter of the invocation is stored in the body of in message
//		inMessage.getBody();
		
	}

}
