package de.test.camel;

import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.model.dataformat.JsonLibrary;

import de.test.api.rest.IRest;

public class RestRouteBuilder extends RouteBuilder{
	
	private Processor restProcessor;
	
	private String restBaseAddress;
	
	@Override
	public void configure() throws Exception {
		String fromURI = buildFromURI(IRest.class);
		
		from(fromURI)
			.process(restProcessor)
			.marshal()
			.json(JsonLibrary.Jackson);
		
	}
	
	public Processor getRestProcessor() {
		return restProcessor;
	}

	public void setRestProcessor(Processor restProcessor) {
		this.restProcessor = restProcessor;
	}

	public String getRestBaseAddress() {
		return restBaseAddress;
	}

	public void setRestBaseAddress(String restBaseAddress) {
		this.restBaseAddress = restBaseAddress;
	}

	@SuppressWarnings("rawtypes")
	private String buildFromURI(Class... resourceClasses){
		StringBuilder sb = new StringBuilder("cxfrs://");
		sb.append(restBaseAddress)
		  .append("?bindingStyle=SimpleConsumer&resourceClasses=");
		
		for(Class resourceClass : resourceClasses){
			sb.append(resourceClass.getName()).append(",");
		}
		
		sb.deleteCharAt(sb.length()-1); //remove last ,

		return sb.toString();
	}

}
